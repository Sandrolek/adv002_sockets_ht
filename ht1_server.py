"""
Задание 1
Создайте UDP сервер, который ожидает сообщения о новых устройствах в сети.
Он принимает сообщения определенного формата, в котором будет
идентификатор устройства и печатает новые подключения в консоль.
Создайте UDP клиента, который будет отправлять уникальный идентификатор
устройства на сервер, уведомляя о своем присутствии.
"""
import socket

cell = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
cell.bind(('0.0.0.0', 65000))

while True:
    print('waiting for incoming connection')
    try:
        user = cell.recv(1024)
        if b'23875923874623' in user:
            print(f"welcome back my old friend {user.decode('utf-8')}")
        else:
            print("Hello stranger")
    except KeyboardInterrupt:
        cell.close()
        break
