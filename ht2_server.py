"""
Задание 2
Создайте UNIX сокет, который принимает сообщение с двумя числами,
разделенными запятой. Сервер должен конвертировать строковое сообщения
в два числа и вычислять его сумму. После успешного вычисления возвращать
ответ клиенту.
"""

import socket

cell = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
cell.bind(('127.0.0.1', 25000))
cell.listen(10)

while True:
    print('waiting for incoming connection')
    conn, addr = cell.accept()
    user_val = []
    user = conn.recv(1024)
    user_str = str(user, encoding=('utf-8'))
    #print(user_str)
    user_val = user_str.split(sep=',')
    #print(user_val)
    user_val1 = (user_val[0].replace("b'",'')).strip()
    user_val2 = (user_val[1].replace("'", '')).strip()
    res = float(user_val1)+float(user_val2)
    print(f"{user_val1}+{user_val2}={res}")
    conn.send(bytes(str(res), encoding='utf-8'))
cell.close()

